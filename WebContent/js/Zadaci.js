function zadatak2() {
    x = document.getElementsByTagName("th");
    for (i = 0; i < x.length; i++) {
        x[i].style.background = "red";
    }
}

function validacija() {
    var form = document.forms[0];
    var naziv = form.naziv.value;

    if (naziv == "") {
        alert("Naziv ne sme biti prazan");
        return false;
    }
    return true;
}

function validacija2() {
    var form = document.forms[0];
    var cena = form.cena.value;

    if (isNaN(cena)) {
        alert("Niste uneli broj!");
        return false;
    } else if (cena <= 0) {
        alert("Cena mora biti veca od 0!");
        return false;
    }
    return validacija();
}

function potvrda() {
    var form = document.forms[0];
    var naziv = form.naziv.value;
    return confirm("Da li ste sigurni da zelite da obrisete: '" + naziv + "'");
}

function promeniTabelu() {
    var forme = document.forms;
    for (i = 0; i < forme.length; i++) {
        var polje = forme[i].komada;
        polje.readOnly = true;

        var btn2 = document.createElement("input");
        btn2.setAttribute('type', 'button');
        btn2.setAttribute("value", "+");
        btn2.setAttribute("onclick", "promeniVrednost(this.parentNode.komada, 1)");
        btn2.style.background = "green";
        forme[i].insertBefore(btn2, polje.nextSibling.nextSibling);


        var btn = document.createElement("input");
        btn.setAttribute('type', 'button');
        btn.setAttribute("value", "-");
        btn.setAttribute("onclick", "promeniVrednost(this.parentNode.komada, -1)");
        btn.style.background = "red";
        forme[i].insertBefore(btn, polje.nextSibling.nextSibling);

    }
}

function promeniVrednost(polje, n) {
    if (!isNaN(polje.value)) {
        var vrednostPolja = parseInt(polje.value) + n;
        if ((vrednostPolja) > 0) {
            polje.value = vrednostPolja;
            console.log(vrednostPolja);
        }
    }
}