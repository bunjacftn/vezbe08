function kontrola() {
    var form = document.forms[0];
    var korisnickoIme = form.korisnickoIme.value;
    var lozinka = form.lozinka.value;
    var ponovljenaLozinka = form.ponovljenaLozinka.value;

    console.log("vrednost korisnickog imena: " + korisnickoIme);
    console.log("vrednost lozinke: " + lozinka);
    console.log("vrednost ponovljene lozinke: " + ponovljenaLozinka);

    if (korisnickoIme == "" || lozinka == "" || ponovljenaLozinka == "") {
        alert("Niste popunili sva polja");
        return false;
    }
    if (lozinka != ponovljenaLozinka) {
        alert("Lozinke se ne podudaraju!");
        return false;
    }
    if (korisnickoIme.match("^[a-zA-Z0-9]+$") == null) {
        alert("Korisnicko ime sme da sadrzi samo alfanumericke karaktere");
        return false;
    }
    return true;
}

function izlazIzPolja(polje) {    
    var korisnickoIme = polje.value;
    if (korisnickoIme == "") {
        polje.parentNode.nextSibling.nextSibling.innerHTML = "Niste popunili polje";
    } 
}

function ulazakUPolje(polje) {
    polje.parentNode.nextSibling.nextSibling.innerHTML = "";
}